import {App, Platform} from 'ionic-angular';
import {StatusBar} from 'ionic-native';
import {TabsPage} from './pages/tabs/tabs';
import {
    FIREBASE_PROVIDERS, defaultFirebase,
    AngularFire, firebaseAuthConfig, AuthProviders,
    AuthMethods
} from 'angularfire2';


@App({
  template: '<div class="wrapper"><ion-nav [root]="rootPage"></ion-nav></div>',
  providers: [
        FIREBASE_PROVIDERS,
        defaultFirebase('https://mlkingh.firebaseio.com/'),
        firebaseAuthConfig({
            provider: AuthProviders.Password,
            method: AuthMethods.Password,
            remember: 'default',
            scope: ['email']
        })
    ],
  config: {
    mode: "md",
    tabbarPlacement: 'top',
    tabbarHighlight: false
  }
})
export class MyApp {
  rootPage: any = TabsPage;

  constructor(platform: Platform) {
    platform.ready().then(() => {
      StatusBar.styleDefault();
    });
  }
}
