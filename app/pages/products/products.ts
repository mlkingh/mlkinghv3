import {Page} from 'ionic-angular';
import {AngularFire, FirebaseAuth, AuthProviders, AuthMethods, FirebaseRef, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import {Observable} from 'rxjs/Rx';

@Page({
  templateUrl: 'build/pages/products/products.html'
})
export class Products{
     public services: FirebaseListObservable<any>;

  authInfo: any
  
  constructor(
        private af: AngularFire,
        public auth: FirebaseAuth) {
              this.services = af.database.list('/services');
  } 

   ngOnInit(){
        this.auth.subscribe((data) => {
            if (data) {
                      this.authInfo = data.password 
                      this.authInfo.displayName = data.password.email
            } else {
                this.authInfo = null
                // this.displayLoginModal()
            }
      });
   }

   add(title: string, text: string, img: string): void{
    this.services.push({
       title,
       text,
       img,
    });
  }

   updateTitle(key: string, newService: string) {
      this.services.update(key, { title: newService });
    }
    
   updateText(key: string, newService: string) {
      this.services.update(key, { text: newService });
    }

    updateImg(key: string, newService: string) {
      this.services.update(key, { img: newService});
    }
    
    deleteService(key: string) {    
      this.services.remove(key); 
    }
}
