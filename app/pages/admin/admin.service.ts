import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {AdminData} from '../admin/admin.model';
import {FirebaseService} from 'firebase-angular2/core';


@Injectable()
export class PostService {

    private static baseUrl = "https://mlkingh.firebaseio.com/";
    public db: Firebase;
    
    constructor(
     public http: Http ) {
            this.db = new Firebase(PostService.baseUrl + 'posts');      
    } 

getPosts(): Observable<AdminData> {
        return Observable.create(observer => {
        let listener = this.db.orderByChild("id").on('child_added', snapshot => {
            let data = snapshot.val();
            observer.next(new AdminData(
            snapshot.key(),
            data.id,
            data.date, 
            data.title,
            data.entry1,
            data.entry2,
            data.excerpt,
            data.img,
            data.img1,
            data.img2,
            data.img3
            ));
        }, observer.error);

        return () => {
            this.db.off('child_added', listener);
        };
        });
    }
};  