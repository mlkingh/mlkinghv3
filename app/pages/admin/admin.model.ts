export class  AdminData{  
  constructor(
    public id: string,
    public order: string,
    public date: string,
    public title: string,
    public entry1: string,
    public entry2: string,
    public excerpt: string,
    public img: string,
    public img1: string,
    public img2: string,
    public img3: string
  ) { }
};