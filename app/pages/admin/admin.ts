import {Page, NavController, Modal, ViewController, Toast, ToastOptions} from 'ionic-angular';
import {Component, OnInit, Inject} from '@angular/core';
import {AngularFire, FirebaseAuth, AuthProviders, AuthMethods, FirebaseRef, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';
import {Subscription} from 'rxjs/Subscription';
import {Login} from '../login/login';
import {Home} from '../home/home';


@Page({
  templateUrl: 'build/pages/admin/admin.html'
})
export class Admin {

    public posts: FirebaseListObservable<any>;

    authInfo: any

    constructor(
        @Inject(FirebaseRef) public ref: Firebase,
        public af: AngularFire,
        public auth: FirebaseAuth,
        public nav: NavController,
        public viewCtrl: ViewController) {
          this.viewCtrl = viewCtrl;
          this.posts = af.database.list('/posts').map( (arr) => { return arr.reverse(); });
      }
    
   ngOnInit(){
        this.auth.subscribe((data) => {
            if (data) {
                      this.authInfo = data.password 
                      this.authInfo.displayName = data.password.email
            } else {
                this.authInfo = null
                // this.displayLoginModal()
            }
      });
   }

   add(id:string, date: string, title: string, entry1: string, entry2: string, excerpt: string, img: string, img1: string, img2: string, img3: string): void{
    this.posts.push({
      id,
      date,
       title,
       entry1,
       entry2,
       excerpt,
       img,
       img1,
       img2,
       img3
    });
    
    let toast = Toast.create({
        message: 'post was added successfully',
        duration: 3000
      });
       this.nav.present(toast);
  }
  updateId(key: string, newPost: string) {
      this.posts.update(key, {id: newPost });
    }
    
   updateDate(key: string, newPost: string) {
      this.posts.update(key, {date: newPost });
    }
    
   updateTitle(key: string, newPost: string) {
      this.posts.update(key, { title: newPost });
    }

  updateEntry1(key: string, newPost: string) {
      this.posts.update(key, { entry1: newPost });
    }

  updateEntry2(key: string, newPost: string) {
      this.posts.update(key, { entry2: newPost });
    }

  updateExcerpt(key: string, newPost: string) {
      this.posts.update(key, { excerpt: newPost });
    }
  updateImg(key: string, newPost: string) {
      this.posts.update(key, { img: newPost });
    }

  updateImg1(key: string, newPost: string) {
      this.posts.update(key, { img1: newPost });
    }

  updateImg2(key: string, newPost: string) {
      this.posts.update(key, { img2: newPost });
    }

  updateImg3(key: string, newPost: string) {
      this.posts.update(key, { img3: newPost });
    }
    
  deletePost(key: string) {    
      this.posts.remove(key); 
    }
    
   displayLoginModal() {
        let loginPage = Modal.create(Login);
        this.nav.present(loginPage);
    }
    
   cancelLoginModal(){
     this.viewCtrl.dismiss();
   }
    
   logoutClicked() {
        if (this.authInfo && (this.authInfo.email ||  this.authInfo.accessToken)) {
            this.auth.logout();
            return;
        };
    }

    goBack(){
      this.nav.pop()
    }
    
}