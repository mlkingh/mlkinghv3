import {Page} from 'ionic-angular';
import {Home} from '../home/home';
import {About} from '../about/about';
import {Products} from '../products/products';
import {Contact} from '../contact/contact';
import {Gallery} from '../gallery/gallery';
import {Admin} from '../admin/admin';
import {PostsPage} from '../posts/posts';

@Page({
  templateUrl: 'build/pages/tabs/tabs.html'
})
export class TabsPage {

  tab1Root: any = Home;
  tab2Root: any = About;
  tab3Root: any = Products;
  tab4Root: any = Contact;
  tab5Root: any = PostsPage;
  tab6Root: any = Gallery;
  tab7Root: any = Admin;
}
