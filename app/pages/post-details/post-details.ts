import {Page, NavController, NavParams} from 'ionic-angular';
import {ViewChild} from '@angular/core';
import {AdminData} from '../admin/admin.model';
import {PostService} from '../admin/admin.service';

@Page({
  templateUrl: 'build/pages/post-details/post-details.html',
    providers: [
    PostService
  ]
})
export class PostDetails{
  @ViewChild('pageslider') slider: Slides;

  private post;
  
  constructor(
    private nav: NavController,
    private navp: NavParams
    ) {
      this.post = navp.get('post')
    }

  ngOnInit(){
      this.post = this.navp.data.selectedPost;
    }

  goBack(){
    this.nav.pop();
  } 

  viewNext(){
      this.slider.slideNext();
    }

  viewPrev(){
     this.slider.slidePrev();
   }
}
