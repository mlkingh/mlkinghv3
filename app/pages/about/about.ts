import {Page} from 'ionic-angular';
import {AngularFire, FirebaseAuth, AuthProviders, AuthMethods, FirebaseRef, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import {Observable} from 'rxjs/Rx';

@Page({
  templateUrl: 'build/pages/about/about.html',
})
export class About {
  
  public abouts: FirebaseListObservable<any>;

  authInfo: any
  
  constructor(
        private af: AngularFire,
        public auth: FirebaseAuth) {
              this.abouts = af.database.list('/abouts');
  } 

   ngOnInit(){
        this.auth.subscribe((data) => {
            if (data) {
                      this.authInfo = data.password 
                      this.authInfo.displayName = data.password.email
            } else {
                this.authInfo = null
                // this.displayLoginModal()
            }
      });
   }

   add(title: string, text: string, img: string): void{
    this.abouts.push({
       title,
       text,
       img,
    });
  }

   updateTitle(key: string, newAbout: string) {
      this.abouts.update(key, { title: newAbout });
    }
    
   updateText(key: string, newAbout: string) {
      this.abouts.update(key, { text: newAbout });
    }

  updateImg(key: string, newAbout: string) {
      this.abouts.update(key, { img: newAbout });
    }
    
    deleteAbout(key: string) {    
      this.abouts.remove(key); 
    }
}
