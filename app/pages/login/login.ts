import {Modal, NavController, Page, ViewController} from 'ionic-angular';
import {Component, OnInit, Inject} from '@angular/core';
import {AngularFire, FirebaseAuth, FirebaseRef, AuthProviders, AuthMethods } from 'angularfire2';

@Page({
  templateUrl: 'build/pages/login/login.html',
})
export class Login{
  
   private error: any;
 
    constructor(public auth: FirebaseAuth,
        @Inject(FirebaseRef) public ref: Firebase,
        public viewCtrl: ViewController) { }
        
   dismiss(){
     this.viewCtrl.dismiss();
   }
   
  registerUser(_credentials, _event) {
        _event.preventDefault();

        this.auth.createUser(_credentials).then((authData: FirebaseAuthData) => {

            _credentials.created = true;

            return this.login(_credentials, _event);

        }).catch((error) => {
            this.error = error
        });
    }
    
    login(credentials, _event) {
        _event.preventDefault();

        let addUser = credentials.created
        credentials.created = null;

        this.auth.login(credentials, {
            provider: AuthProviders.Password,
            method: AuthMethods.Password
        }).then((authData) => {

            if (addUser) {
                var auth: FirebaseAuthDataPassword = authData.password
                return this.ref.child('users')
                    .child(authData.uid)
                    .set({
                        "provider": authData.provider,
                        "avatar": auth.profileImageURL,
                        "displayName": auth.email,
                        "authData" : authData
                    })
            } else {
                this.dismiss()
            }

        }).then((value) => {
            this.dismiss()
        }).catch((error) => {
            this.error = error
        });
    }
    
}
