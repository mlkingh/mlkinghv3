import {Page, NavController, Modal, ViewController, Toast, ToastOptions} from 'ionic-angular';
import {Component, OnInit, Inject, ViewChild} from '@angular/core';
import {AngularFire, FirebaseAuth, AuthProviders, AuthMethods, FirebaseRef, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';
import {Subscription} from 'rxjs/Subscription';
import {Login} from '../login/login';

@Page({
  templateUrl: 'build/pages/gallery/gallery.html',
})
export class Gallery {
@ViewChild('pageslider') slider: Slides;

mySlideOptions = { 
   //put slide options here
  };

public images: FirebaseListObservable<any>;

  authInfo: any
  
  constructor(
        private af: AngularFire,
        public auth: FirebaseAuth) {
              this.images = af.database.list('/images');
   } 

   ngOnInit(){
        this.auth.subscribe((data) => {
            if (data) {
                      this.authInfo = data.password 
                      this.authInfo.displayName = data.password.email
            } else {
                this.authInfo = null
                // this.displayLoginModal()
            }
      });
   }

   add(img: string): void{
    this.images.push({
       img,
    });
   }

  updateImg(key: string, newImage: string) {
      this.images.update(key, { img: newImage});
    }
    
    deleteImage(key: string) {    
      this.images.remove(key); 
    }

  viewNext(){
      this.slider.slideNext();
    }

  viewPrev(){
     this.slider.slidePrev();
   }

  //  viewImage(index:number){
  //    let Index = 0;
  //    let length = this.slider.length();
  //    let currentIndex = this.slider.getActiveIndex();
  //    if (currentIndex >= Index){
  //         this.slider.slideTo(currentIndex + 1);
  //     }
  // }
}