import {Page, NavController, Modal, ViewController, } from 'ionic-angular';
import {Admin} from '../admin/admin';

@Page({
  templateUrl: 'build/pages/home/home.html',
})
export class Home{
  constructor(
    public nav: NavController,
   public viewCtrl: ViewController) {}

   goLogin(){
     this.nav.push(Admin);
   }
}
