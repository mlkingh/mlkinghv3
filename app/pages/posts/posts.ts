import {Page, NavController, NavParams} from 'ionic-angular';
import {Subscription} from 'rxjs/Subscription';
import {PostDetails} from '../post-details/post-details';
import {AdminData} from '../admin/admin.model';
import {PostService} from '../admin/admin.service';


@Page({
  templateUrl: 'build/pages/posts/posts.html',
  providers: [
    PostService
  ]
})
export class PostsPage {
  public posts: AdminData[] = [];
  public subscription: Subscription;

  constructor(private _svc: PostService, private _navController: NavController, private navP: NavParams) {}

  ngOnInit(){
        this.subscription = this._svc.getPosts().subscribe(post => {
        this.posts.push(post);
    });
  }

 ngOnDestroy(){
    this.subscription.unsubscribe();
 }

  selectPost(post){
        this._navController.push(PostDetails, {
            selectedPost: post
        });
  }
}
