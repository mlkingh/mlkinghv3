import {Page, NavController} from 'ionic-angular';
import {AngularFire, FirebaseAuth, AuthProviders, AuthMethods, FirebaseRef, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import {Observable} from 'rxjs/Rx';

@Page({
  templateUrl: 'build/pages/contact/contact.html',
})

export class Contact {
  public contacts: FirebaseListObservable<any>;

  authInfo: any
  
  constructor(
        private af: AngularFire,
        public auth: FirebaseAuth) {
              this.contacts = af.database.list('/contacts');
  } 

   ngOnInit(){
        this.auth.subscribe((data) => {
            if (data) {
                      this.authInfo = data.password 
                      this.authInfo.displayName = data.password.email
            } else {
                this.authInfo = null
                // this.displayLoginModal()
            }
      });
   }

   add(title: string, text: string, img: string): void{
    this.contacts.push({
       title,
       text,
       img,
    });
  }

   updateTitle(key: string, newContact: string) {
      this.contacts.update(key, { title: newContact });
    }
    
   updateText(key: string, newContact: string) {
      this.contacts.update(key, { text: newContact });
    }
    
    updateImg(key: string, newContact: string) {
      this.contacts.update(key, { img: newContact});
    }

    deleteContact(key: string) {    
      this.contacts.remove(key); 
    }
}
